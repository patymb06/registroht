 # Registro de Humedad y Temperatura  
**Con el uso de NetBeans y Java, conectado a database y página web.**

Para el uso de este proyecto con la finalidad de facilitar el monitoreo de plantas (tanto en lugares cerrados como abiertos), con una interfaz amigable al usuario se realizo tener el registro de los usuarios y el registro de monitoreo de las variables. 
Se hizo el uso de:
- NetBeans (Interfaz Gráfica y Programa Principal).
- Java (Lenguaje de programación utilizado).
- XAMPP (Servidor local).
- Libreria de Arduino para Java (PanamaHitck y RS2XML).
- Libreria de Arduino para Temperatura y Humedad (ESPAsyncWebServer y ESPAsyncTCP).
- Uso de HTML y PHP.
